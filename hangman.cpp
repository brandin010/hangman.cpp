//hangman vesion 
#include <iostream>
#include <string> //enables the use of strings 
#include <vector> //enables the use of vectors
#include <algorithm>//enables 
#include <ctime> //enables using time to seed rand
#include <cctype> //enables us to compaer apples to apples.


    
 


int main()
{
    //setup
    std::cout << "Please enter your name followed by enter: ";
    std::string name;
    std::cin >> name;
    const int MAX_WRONG = 8;  

    std::vector <std::string> words; //store the words 
    words.push_back("FROG");
    words.push_back("LONELY");
    words.push_back("DEPRESSION");
    words.push_back("DEAD");
    words.push_back("ALIVE");

    srand(static_cast<unsigned int>(time(0)));
    std::random_shuffle(words.begin(), words.end());
    const std::string THE_WORD = words[0]; //word to guess
    int wrong = 0; //number of wrong guesses
    std::string soFar(THE_WORD.size(), '-');//words guessed so far
    std::string used = "";//letters guessed already
    std::cout <<"\nWelcome to hangman " << name << std::endl;


    //main loop

    while ((wrong < MAX_WRONG) && (soFar != THE_WORD))
    {
        std::cout << "\nYou have " << (MAX_WRONG - wrong);
        std::cout << " incorrect guesses left\n" <<std::endl;
        std::cout << "You've used the following letters:\n" << used << std::endl;
        std::cout << "\nSo far, the word is:\n" << soFar << std::endl;
        char guess;
        std::cout << "\nPlease enter your guess:";
        std::cin >> guess;
        guess = toupper(guess);

        while (used.find(guess) != std::string::npos)
        {
            std::cout << "\nYou've already guessed " << guess << std::endl;
            std::cout << "Enter your guess: ";
            std::cin >> guess;
            guess = toupper(guess);
        }
        
        used += guess;

        if (THE_WORD.find(guess) != std::string::npos)
        {
            std::cout << "\nThats right! " << guess <<" is in the word.\n";
            //update soFar to include newly guessed letter
            for (int i = 0; i <THE_WORD.length(); ++i)
            {
                if (THE_WORD[i] == guess)
                {
                    soFar[i] = guess;
                }
            }
        }
        else 
        {
            std::cout <<"Sorry, " << guess << " isn't in the word.\n";
            ++wrong;
        }
    }
    //end game
    if (wrong == MAX_WRONG)
    {
        std::cout <<"\nYou've been hanged!" << std::endl;
    }
    else
    {
        std::cout << "\nYou've guessed it!";
    }
    
    //give the word
    std::cout << "\n\nThe word was: " << THE_WORD << std::endl;
    system("pause");
    return 0;
    
} 


